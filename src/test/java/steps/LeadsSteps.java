package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LeadsSteps {
	public ChromeDriver driver;

@Given("open the Browser")
public void openTheBrowser() {
    driver = new ChromeDriver();
}

@Given("Load the URL")
public void loadTheURL() {
    driver.get("http://leaftaps.com/opentaps/control/login");
}

@Given("Max the Browser")
public void maxTheBrowser() {
    driver.manage().window().maximize();
}

@Given("Set the Timeouts")
public void setTheTimeouts() {
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

@Given("Enter the Username as (.*)")
public void enterTheUsername(String data) {
	driver.findElement(By.id("username")).sendKeys(data);
}

@Given("Enter the Password as (.*)")
public void enterThePassword(String data) {
	driver.findElement(By.id("password")).sendKeys(data);
}

@Given("Click on Login Button")
public void clickOnLoginButton() {
	driver.findElement(By.className("decorativeSubmit")).click();
}

@Given("Click on CRMSFA")
public void clickOnCRMSFA() {
	driver.findElement(By.linkText("CRM/SFA")).click();
}

@Given("Click on Leads")
public void clickOnLeads() {
	driver.findElement(By.linkText("Leads")).click();
}

@Given("Click on CreateLead")
public void clickOnCreateLead() {
	driver.findElement(By.linkText("Create Lead")).click();
}

@Given("Enter company name as (.*)")
public void enterCompanyName(String data) {
	driver.findElement(By.id("createLeadForm_companyName")).sendKeys(data);
}

@Given("Enter First name as (.*)")
public void enterFirstName(String data) {
	driver.findElement(By.id("createLeadForm_firstName")).sendKeys(data);
}

@Given("Enter Last name as (.*)")
public void enterLastName(String data) {
	driver.findElement(By.id("createLeadForm_lastName")).sendKeys(data);
}

@When("Click on the Create Lead Button")
public void clickOnTheCreateLeadButton() {
	driver.findElement(By.name("submitButton")).click();
}

@Then("Verify the Lead Creation")
public void verifyTheLeadCreation() {
	String asdf = driver.findElement(By.id("viewLead_companyName_sp")).getText().replaceAll("\\D", "");
	System.out.println(asdf);
}
@Then("Verify Login Successfull")
public void loginsuccess() {
System.out.println("Login Successful");
}

@But("Verify Login fails")
public void loginfails() {
	System.out.println("Login Failure");

}

}
