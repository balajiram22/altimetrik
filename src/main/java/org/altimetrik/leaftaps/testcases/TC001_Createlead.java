package org.altimetrik.leaftaps.testcases;

import org.altimetrik.leaftaps.base.ProjectBase;
import org.altimetrik.leaftaps.pages.LoginPage;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC001_Createlead<driver> extends ProjectBase {
	public TC001_Createlead(RemoteWebDriver driver) {
		
	}
	@BeforeClass
	public void setData() {
		excelFileName="TC001";
	}
	
@Test(dataProvider = "getData")
public void reunCreateLead(String username, String password, String compName, String firstName, String lastName) {
	new LoginPage(driver)
	.enterUsername(username)
	.enterPassword(password)
	.clickLoginButton()
	.clickCRMSFA()
	.clickLeads()
	.clickCreateLead()
	.enterCompanyName(compName)
	.enterFirstName(firstName)
	.enterLastName(lastName)
	.clickCreateLeadButton();
	
	
}
}
