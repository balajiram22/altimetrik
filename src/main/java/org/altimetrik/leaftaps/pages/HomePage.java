package org.altimetrik.leaftaps.pages;

import org.altimetrik.leaftaps.base.ProjectBase;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends ProjectBase{
	
	public HomePage() {PageFactory.initElements(driver, this);}
	
	public LeadsPage clickLeads() {
		driver.findElementByLinkText("Leads").click();
		return new LeadsPage();
	}
	

}
