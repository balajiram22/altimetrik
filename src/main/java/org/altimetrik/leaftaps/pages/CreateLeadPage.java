package org.altimetrik.leaftaps.pages;

import org.altimetrik.leaftaps.base.ProjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

public class CreateLeadPage extends ProjectBase{

	public CreateLeadPage() {PageFactory.initElements(driver, this);}
	public CreateLeadPage enterCompanyName(String data) {
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys(data);
		return this;
	}
	public CreateLeadPage enterFirstName(String data) {
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys(data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys(data);
		return this;

	}
	public  ViewLeadPage clickCreateLeadButton() {
		driver.findElement(By.name("submitButton")).click();
		return new ViewLeadPage();

	}


}
