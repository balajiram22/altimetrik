package org.altimetrik.leaftaps.pages;

import org.altimetrik.leaftaps.base.ProjectBase;
import org.openqa.selenium.support.PageFactory;

public class LeadsPage extends ProjectBase{
	
	public LeadsPage() {PageFactory.initElements(driver, this);}
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
	}
	

}
