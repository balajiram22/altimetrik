package org.altimetrik.leaftaps.pages;

import org.altimetrik.leaftaps.base.ProjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends ProjectBase{
	
	public LoginPage(RemoteWebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	@FindAll({@FindBy(how=How.ID, using="username"),@FindBy(how=How.ID, using="French")}) 
	WebElement eleUsername;
	
	public LoginPage enterUsername(String data) {
//		driver.findElementById("username").sendKeys(data);
		eleUsername.sendKeys(data);
		return this;
	}
	
	public LoginPage enterPassword(String data) {
		driver.findElementById("password").sendKeys(data);
		return this;
	}
	
	public MyHomePage clickLoginButton() {
		driver.findElement(By.className("decorativeSubmit")).click();
		return new MyHomePage();
	}
	
}
