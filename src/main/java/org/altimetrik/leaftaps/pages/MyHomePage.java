package org.altimetrik.leaftaps.pages;

import org.altimetrik.leaftaps.base.ProjectBase;
import org.openqa.selenium.support.PageFactory;

public class MyHomePage extends ProjectBase{
	
	public MyHomePage() {PageFactory.initElements(driver, this);}
	public HomePage clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
		return new HomePage();
	}
	

}
