package org.altimetrik.leaftaps.base;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import utils.ReadLibrary;

public class ProjectBase {
	public String excelFileName;
	public static ChromeDriver driver;
	@BeforeMethod
	public void startApp() {
		driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@AfterMethod
	public void closeApp() {
		driver.close();
	}
	@DataProvider(name="getData")
	public String[][] dataSupplier() throws IOException {
		return ReadLibrary.readExcelData(excelFileName);

	}
	
	
	
	
	
	

}
